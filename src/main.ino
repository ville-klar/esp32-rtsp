/*#define SOFTAP_MODE*/

#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>

#include "OV2640Streamer.h"
#include "CRtspSession.h"
#include "OV2640.h"

#include "custom_camera.h"

OV2640 cam;
WiFiServer rtspServer(8554);
CStreamer *streamer;
CRtspSession *session;
WiFiClient client; 

#ifdef SOFTAP_MODE
IPAddress apIP = IPAddress(192, 168, 1, 1);
#else
/*#include "wifikeys.h"*/
const char* ssid = "SSID";
const char* password = "password";
#endif

const uint32_t  msecPerFrame = 50;

// Globals
WebSocketsServer webSocket = WebSocketsServer(80);

// Called when receiving any WebSocket message
void onWebSocketEvent(uint8_t num,
    WStype_t type,
    uint8_t * payload,
    size_t length) {
    // Figure out the type of WebSocket event
    switch(type) {
        // Client has disconnected
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
            // New client has connected
        case WStype_CONNECTED:
            {
                IPAddress ip = webSocket.remoteIP(num);
                Serial.printf("[%u] Connection from ", num);
                Serial.println(ip.toString());
            }
            break;
            // Echo text message back to client
        case WStype_TEXT:
            Serial.printf("[%u] Text: %s\n", num, payload);
            webSocket.sendTXT(num, "OBJ");
            break;
            // For everything else: do nothing
        case WStype_BIN:
        case WStype_ERROR:
        case WStype_FRAGMENT_TEXT_START:
        case WStype_FRAGMENT_BIN_START:
        case WStype_FRAGMENT:
        case WStype_FRAGMENT_FIN:
        default:
            break;
    }
}

void SocketTask( void * pvParameters ){ 
    for(;;){
        webSocket.loop();
        delay(100); 
    }
}


void setup()
{
    Serial.begin(115200);
    cam.init(custom_camera_config);
    #ifdef SOFTAP_MODE
    const char *hostname = "devcam";
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    bool result = WiFi.softAP(hostname, "12345678", 1, 0);
    if (!result)
    {
        Serial.println("AP Config failed.");
        return;
    }
    else
    {
        Serial.println("AP Config Success.");
        Serial.print("AP MAC: ");
        Serial.println(WiFi.softAPmacAddress());
    }
    #else
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(F("."));
    }
    #endif
    Serial.println(WiFi.localIP().toString());
    rtspServer.begin();
    webSocket.begin();
    webSocket.onEvent(onWebSocketEvent);
    xTaskCreatePinnedToCore(
            SocketTask,		//	[> Function to implement the task <]
            "SocketTask",		//	[> Name of the task <]
            4096,		//	[> Stack size in words <]
            NULL,		//	[> Task input parameter <]
            2,			//	[> Priority of the task <]
            NULL,		//	[> Task handle. <]
            1);	//    	[> Core where the task should run <]
}

void loop()
{
    static uint32_t lastimage = millis();
    if(session) {
        session->handleRequests(0); // we don't use a timeout here,
        uint32_t now = millis();
        if(now > lastimage + msecPerFrame || now < lastimage) { // handle clock rollover
            session->broadcastCurrentFrame(now);
            lastimage = now;
            // check if we are overrunning our max frame rate
            now = millis();
            if(now > lastimage + msecPerFrame)
                printf("warning exceeding max frame rate of %d ms\n", now - lastimage);
        }

        if(session->m_stopped) {
            delete session;
            delete streamer;
            session = NULL;
            streamer = NULL;
        }
    }
    else {
        client = rtspServer.accept();
        if(client) {
            streamer = new OV2640Streamer(&client, cam);             // our streamer for UDP/TCP based RTP transport
            session = new CRtspSession(&client, streamer); // our threads RTSP session and state
        }
    }
}
