import websocket
import imutils
import cv2
from datetime import datetime
img = cv2.imread('testImage.png')
img = imutils.resize(img, width=1200)
    
esp_ip = "192.168.1.45"
# esp_ip = "192.168.1.1"
ws = websocket.WebSocket()
ws.connect("ws://"+esp_ip)
vcap = cv2.VideoCapture("rtsp://"+esp_ip+":8554/mjpeg/1")

#range-detector -f HSV -i sampleimage.png
redLower = (30, 10, 110)
redUpper = (130, 50, 130)

i = 0

while(1):
    i += 1
    if (i == 10):                    #let the loop run 10 times before showing the test image
        start = datetime.now()
        cv2.imshow('image',img)
        print("Showing test image")
    ret, frame = vcap.read()
    frame = imutils.resize(frame, width=800)
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    maskred = cv2.inRange(frame, redLower, redUpper)
    maskred = cv2.erode(maskred, None, iterations=2)
    maskred = cv2.dilate(maskred, None, iterations=2)
    cnts_red = cv2.findContours(maskred.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None
    if len(cnts_red) > 0:
        c = max(cnts_red, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        if radius > 25:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
            cv2.circle(frame, center, 5, (0, 0, 255), -1)
            ws.send("object!")
            result = ws.recv()
            if(result == "OBJ"):
                try:
                    start
                except NameError:
                    print ("start not defined")
                else:
                    stop = datetime.now()
                    seconds = (stop-start).seconds
                    millis  = (stop-start).microseconds/1000
                    print("Recognized color and got confirmation from ESP32 in %d.%d s" % (seconds,millis))
                    break
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

vcap.release()
cv2.destroyAllWindows()
ws.close()
