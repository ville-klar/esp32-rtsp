#pragma once

//FRAMESIZE_QQVGA,    // 160x120
//FRAMESIZE_QQVGA2,   // 128x160
//FRAMESIZE_QCIF,     // 176x144
//FRAMESIZE_HQVGA,    // 240x176
//FRAMESIZE_QVGA,     // 320x240
//FRAMESIZE_CIF,      // 400x296
//FRAMESIZE_VGA,      // 640x480
//FRAMESIZE_SVGA,     // 800x600
//FRAMESIZE_XGA,      // 1024x768
//FRAMESIZE_SXGA,     // 1280x1024
//FRAMESIZE_UXGA, // 1600x1200

camera_config_t custom_camera_config{
    .pin_pwdn = 32,
    .pin_reset = -1,
    .pin_xclk = 0,
    .pin_sscb_sda = 26,
    .pin_sscb_scl = 27,
    .pin_d7 = 35,
    .pin_d6 = 34,
    .pin_d5 = 39,
    .pin_d4 = 36,
    .pin_d3 = 21,
    .pin_d2 = 19,
    .pin_d1 = 18,
    .pin_d0 = 5,
    .pin_vsync = 25,
    .pin_href = 23,
    .pin_pclk = 22,
    .xclk_freq_hz = 20000000,
    .ledc_timer = LEDC_TIMER_1,
    .ledc_channel = LEDC_CHANNEL_1,
    .pixel_format = PIXFORMAT_JPEG,
    // .frame_size = FRAMESIZE_UXGA, // needs 234K of framebuffer space
    // .frame_size = FRAMESIZE_SXGA, // needs 160K for framebuffer
    // .frame_size = FRAMESIZE_XGA, // needs 96K or even smaller FRAMESIZE_SVGA - can work if using only 1 fb
    .frame_size = FRAMESIZE_VGA,
    .jpeg_quality = 12, //0-63 lower numbers are higher quality
    .fb_count = 2       // if more than one i2s runs in continous mode.  Use only with jpeg
};


